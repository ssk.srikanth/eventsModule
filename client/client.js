'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'mod.nbos',
        'mod.idn',
        'mod.m113',
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: '602831fe-3053-4ae8-ab6b-7d462e9cbdba',
        CLIENT_SECRET: 'myModule',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
