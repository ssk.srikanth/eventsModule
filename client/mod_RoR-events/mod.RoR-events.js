angular.module('mod.m113', []);

angular.module('mod.m113')
  .constant('MOD_RoR-events', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m113', {
                url: '/RoR-events',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m113.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_RoR-events/views/menu/RoR-events.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "RoR-events"
               }
           })
            .state('m113.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_RoR-events/views/dashboard/RoR-events.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "RoR-events"
                }

            })
            .state('m113.events', {
                url: '/events',
                templateUrl: "mod_RoR-events/views/events/events.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Events",
                    module: "RoR-events"
                }

            })

            //FOR APP ADMIN
           .state('m113.admin', {
               url: '/admin',
               templateUrl: "mod_RoR-events/views/admin/RoR-events.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "RoR-events"
               }
           })

    }])